/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Copyright (C) 2021 https://ghintech.com and contributors (see README.md file).
 */

package com.ghintech.infor.logistics.component;

import com.ghintech.infor.logistics.base.CustomModelFactory;
import com.ghintech.infor.logistics.model.M_INF_Dispatch;
import com.ghintech.infor.logistics.model.M_INF_Picking;

/**
 * Model Factory
 */
public class ModelFactory extends CustomModelFactory {

	/**
	 * For initialize class. Register the models to build
	 * 
	 * <pre>
	 * protected void initialize() {
	 * 	registerModel(MTableExample.Table_Name, MTableExample.class);
	 * }
	 * </pre>
	 */
	@Override
	protected void initialize() {
		registerModel(M_INF_Picking.Table_Name, M_INF_Picking.class);
		registerModel(M_INF_Dispatch.Table_Name, M_INF_Dispatch.class);
	}

}
