/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Copyright (C) 2021 https://ghintech.com and contributors (see README.md file).
 */

package com.ghintech.infor.logistics.component;

import org.adempiere.base.event.IEventTopics;
import org.compiere.model.MInOut;
import org.compiere.model.MInvoice;

import com.ghintech.infor.logistics.base.CustomEventFactory;
import com.ghintech.infor.logistics.event.DeleteInOutPickingNumber;
import com.ghintech.infor.logistics.event.DeleteInvoiceDispatchNumber;
import com.ghintech.infor.logistics.event.InsertInOutPickingNumber;
import com.ghintech.infor.logistics.event.InsertInvoiceDispatchNumber;
import com.ghintech.infor.logistics.event.UpdateInOutPickingNumber;
import com.ghintech.infor.logistics.event.UpdateInvoiceDispatchNumber;
import com.ghintech.infor.logistics.model.M_INF_Dispatch;
import com.ghintech.infor.logistics.model.M_INF_Picking;

/**
 * Event Factory
 */
public class EventFactory extends CustomEventFactory {

	/**
	 * For initialize class. Register the custom events to build
	 * 
	 * <pre>
	 * protected void initialize() {
	 * 	registerEvent(IEventTopics.DOC_BEFORE_COMPLETE, MTableExample.Table_Name, EPrintPluginInfo.class);
	 * }
	 * </pre>
	 */
	@Override
	protected void initialize() {
		registerEvent(IEventTopics.PO_AFTER_NEW,M_INF_Picking.Table_Name,InsertInOutPickingNumber.class);
		registerEvent(IEventTopics.PO_AFTER_CHANGE,M_INF_Picking.Table_Name,UpdateInOutPickingNumber.class);
		registerEvent(IEventTopics.PO_AFTER_NEW,M_INF_Dispatch.Table_Name,InsertInvoiceDispatchNumber.class);
		registerEvent(IEventTopics.PO_AFTER_CHANGE,M_INF_Dispatch.Table_Name,UpdateInvoiceDispatchNumber.class);
		registerEvent(IEventTopics.PO_BEFORE_CHANGE,MInOut.Table_Name,DeleteInOutPickingNumber.class);
		registerEvent(IEventTopics.PO_BEFORE_CHANGE,MInvoice.Table_Name,DeleteInvoiceDispatchNumber.class);
	}

}
