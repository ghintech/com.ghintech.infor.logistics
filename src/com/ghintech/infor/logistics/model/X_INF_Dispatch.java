/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package com.ghintech.infor.logistics.model;

import java.sql.ResultSet;
import java.util.Properties;
import org.compiere.model.*;

/** Generated Model for INF_Dispatch
 *  @author iDempiere (generated) 
 *  @version Release 8.2 - $Id$ */
public class X_INF_Dispatch extends PO implements I_INF_Dispatch, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20210602L;

    /** Standard Constructor */
    public X_INF_Dispatch (Properties ctx, int INF_Dispatch_ID, String trxName)
    {
      super (ctx, INF_Dispatch_ID, trxName);
      /** if (INF_Dispatch_ID == 0)
        {
			setINF_Dispatch_ID (0);
        } */
    }

    /** Load Constructor */
    public X_INF_Dispatch (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuilder sb = new StringBuilder ("X_INF_Dispatch[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	/** Set Description.
		@param Description 
		Optional short description of the record
	  */
	public void setDescription (String Description)
	{
		set_Value (COLUMNNAME_Description, Description);
	}

	/** Get Description.
		@return Optional short description of the record
	  */
	public String getDescription () 
	{
		return (String)get_Value(COLUMNNAME_Description);
	}

	/** Set Dispatch List.
		@param INF_Dispatch_ID Dispatch List	  */
	public void setINF_Dispatch_ID (int INF_Dispatch_ID)
	{
		if (INF_Dispatch_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_INF_Dispatch_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_INF_Dispatch_ID, Integer.valueOf(INF_Dispatch_ID));
	}

	/** Get Dispatch List.
		@return Dispatch List	  */
	public int getINF_Dispatch_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_INF_Dispatch_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set INF_Dispatch_UU.
		@param INF_Dispatch_UU INF_Dispatch_UU	  */
	public void setINF_Dispatch_UU (String INF_Dispatch_UU)
	{
		set_Value (COLUMNNAME_INF_Dispatch_UU, INF_Dispatch_UU);
	}

	/** Get INF_Dispatch_UU.
		@return INF_Dispatch_UU	  */
	public String getINF_Dispatch_UU () 
	{
		return (String)get_Value(COLUMNNAME_INF_Dispatch_UU);
	}

	public org.compiere.model.I_M_Shipper getM_Shipper() throws RuntimeException
    {
		return (org.compiere.model.I_M_Shipper)MTable.get(getCtx(), org.compiere.model.I_M_Shipper.Table_Name)
			.getPO(getM_Shipper_ID(), get_TrxName());	}

	/** Set Shipper.
		@param M_Shipper_ID 
		Method or manner of product delivery
	  */
	public void setM_Shipper_ID (int M_Shipper_ID)
	{
		if (M_Shipper_ID < 1) 
			set_Value (COLUMNNAME_M_Shipper_ID, null);
		else 
			set_Value (COLUMNNAME_M_Shipper_ID, Integer.valueOf(M_Shipper_ID));
	}

	/** Get Shipper.
		@return Method or manner of product delivery
	  */
	public int getM_Shipper_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Shipper_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set No. Guía de Despacho.
		@param n_guia_despacho No. Guía de Despacho	  */
	public void setn_guia_despacho (String n_guia_despacho)
	{
		set_Value (COLUMNNAME_n_guia_despacho, n_guia_despacho);
	}

	/** Get No. Guía de Despacho.
		@return No. Guía de Despacho	  */
	public String getn_guia_despacho () 
	{
		return (String)get_Value(COLUMNNAME_n_guia_despacho);
	}

	/** Set Processed.
		@param Processed 
		The document has been processed
	  */
	public void setProcessed (boolean Processed)
	{
		set_Value (COLUMNNAME_Processed, Boolean.valueOf(Processed));
	}

	/** Get Processed.
		@return The document has been processed
	  */
	public boolean isProcessed () 
	{
		Object oo = get_Value(COLUMNNAME_Processed);
		if (oo != null) 
		{
			 if (oo instanceof Boolean) 
				 return ((Boolean)oo).booleanValue(); 
			return "Y".equals(oo);
		}
		return false;
	}

	/** Set Search Key.
		@param Value 
		Search key for the record in the format required - must be unique
	  */
	public void setValue (String Value)
	{
		set_Value (COLUMNNAME_Value, Value);
	}

	/** Get Search Key.
		@return Search key for the record in the format required - must be unique
	  */
	public String getValue () 
	{
		return (String)get_Value(COLUMNNAME_Value);
	}
}