package com.ghintech.infor.logistics.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MInvoice;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import com.ghintech.infor.logistics.base.CustomProcess;
import com.ghintech.infor.logistics.model.M_INF_Dispatch;

public class UpdateDispatchNumberOnInvoice extends CustomProcess{

	private int p_C_Invoice_ID=0;
	private int p_INF_Dispatch_ID=0;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("C_Invoice_ID"))
				p_C_Invoice_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_INF_Dispatch_ID=getRecord_ID();
		
	}

	@Override
	protected String doIt() throws Exception {
		boolean complete_documents = MSysConfig.getBooleanValue("Infor_Logistics_Complete_Invoice",false,this.getAD_Client_ID());
		String docstatuswhere="c_invoice.docstatus = 'IP' or c_invoice.docstatus = 'DR'";
		if(complete_documents)
			docstatuswhere="c_invoice.docstatus='CO'";
		
		String sqlwhere=" c_invoice.C_DocTypeTarget_ID IN (SELECT C_DocType_ID FROM C_DocType WHERE  DocBaseType='ARI' and DocSubInvoiceInv='I') and c_invoice.issotrx = 'Y' and ("+docstatuswhere+") and (INF_Dispatch_ID is null or INF_Dispatch_ID=0)";
		
		if(p_C_Invoice_ID>0)
			sqlwhere =sqlwhere+" AND C_Invoice_ID="+p_C_Invoice_ID;
			
		List<MInvoice> listinvoice=new Query(getCtx(), MInvoice.Table_Name, sqlwhere , get_TrxName())
				.setClient_ID().list();
		M_INF_Dispatch dispatch = new M_INF_Dispatch(getCtx(), p_INF_Dispatch_ID, get_TrxName());
		
		for(MInvoice invoice : listinvoice) {
			invoice.set_ValueOfColumn("n_guia_despacho", dispatch.getValue());
			invoice.set_ValueOfColumn("INF_Dispatch_ID", dispatch.getINF_Dispatch_ID());
			invoice.saveEx(get_TrxName());
		}
			
		return null;
	}

}
