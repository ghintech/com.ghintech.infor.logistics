package com.ghintech.infor.logistics.process;

import java.util.List;
import java.util.logging.Level;

import org.compiere.model.MInOut;
import org.compiere.model.MSysConfig;
import org.compiere.model.Query;
import org.compiere.process.ProcessInfoParameter;
import com.ghintech.infor.logistics.base.CustomProcess;
import com.ghintech.infor.logistics.model.M_INF_Picking;

public class UpdatePickingNumberOnInOut extends CustomProcess{

	private int p_M_InOut_ID=0;
	private int p_INF_Picking_ID=0;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("M_InOut_ID"))
				p_M_InOut_ID = para[i].getParameterAsInt();
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		p_INF_Picking_ID=getRecord_ID();
		
	}

	@Override
	protected String doIt() throws Exception {
		
		boolean complete_documents = MSysConfig.getBooleanValue("Infor_Logistics_Complete_InOut",false,this.getAD_Client_ID());
		String docstatuswhere="m_inout.docstatus='DR' or m_inout.docstatus='IP'";
		if(complete_documents)
			docstatuswhere="m_inout.docstatus='CO'";
		
		String sqlwhere="m_inout.issotrx='Y' and ("+docstatuswhere+")";
		
		if(p_M_InOut_ID>0)
			sqlwhere =sqlwhere+" AND M_InOut_ID="+p_M_InOut_ID;
			
		List<MInOut> listinout=new Query(getCtx(), MInOut.Table_Name, sqlwhere , get_TrxName())
				.setClient_ID().list();
		M_INF_Picking picking = new M_INF_Picking(getCtx(), p_INF_Picking_ID, get_TrxName());
		
		for(MInOut inout : listinout) {
			inout.set_ValueOfColumn("n_guia_preparacion", picking.getValue());
			inout.set_ValueOfColumn("INF_Picking_ID", picking.getINF_Picking_ID());
			inout.saveEx(get_TrxName());
		}
			
		return null;
	}

}
