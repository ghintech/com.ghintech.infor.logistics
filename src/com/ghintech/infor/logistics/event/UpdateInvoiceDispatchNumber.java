package com.ghintech.infor.logistics.event;

import java.util.List;



import org.compiere.model.MInvoice;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;

import com.ghintech.infor.logistics.base.CustomEvent;
import com.ghintech.infor.logistics.model.M_INF_Dispatch;
import org.compiere.model.Query;
public class UpdateInvoiceDispatchNumber extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		// TODO Auto-generated method stub
		boolean complete_documents = MSysConfig.getBooleanValue("Infor_Logistics_Complete_Invoice",false,getPO().getAD_Client_ID());
		String docstatuswhere="c_invoice.docstatus = 'IP' or c_invoice.docstatus = 'DR'";
		if(complete_documents)
			docstatuswhere="c_invoice.docstatus='CO'";
		
		M_INF_Dispatch dispatch = (M_INF_Dispatch)  getPO();
		List<MInvoice> listinvoice=new Query(Env.getCtx(), MInvoice.Table_Name, " c_invoice.C_DocTypeTarget_ID IN (SELECT C_DocType_ID FROM C_DocType WHERE  DocBaseType='ARI' and DocSubInvoiceInv='I') and c_invoice.issotrx = 'Y' and ("+docstatuswhere+")  and INF_Dispatch_ID=?", getPO().get_TrxName())
				.setClient_ID().setParameters(dispatch.get_ID()).list();
		for(MInvoice invoice : listinvoice) {
			invoice.set_ValueOfColumn("n_guia_despacho", dispatch.getValue());
			
			invoice.saveEx(getPO().get_TrxName());
		}
		
	}

}
