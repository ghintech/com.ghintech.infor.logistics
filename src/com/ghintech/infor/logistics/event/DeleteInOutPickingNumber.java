package com.ghintech.infor.logistics.event;

import org.compiere.model.MInOut;
import com.ghintech.infor.logistics.base.CustomEvent;
public class DeleteInOutPickingNumber extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		// TODO Auto-generated method stub
		MInOut inout = (MInOut)  getPO();
		if(inout.get_ValueAsString("n_guia_preparacion").trim().compareTo("")==0 && inout.get_Value("INF_Picking_ID")!=null) {
			inout.set_ValueOfColumn("INF_Picking_ID", null);
			inout.set_ValueOfColumn("n_guia_preparacion", null);
			inout.saveEx();
		}
				
	}

}
