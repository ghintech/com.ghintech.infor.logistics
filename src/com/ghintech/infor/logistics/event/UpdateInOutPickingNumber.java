package com.ghintech.infor.logistics.event;

import java.util.List;



import org.compiere.model.MInOut;
import org.compiere.model.MSysConfig;
import org.compiere.util.Env;

import com.ghintech.infor.logistics.base.CustomEvent;
import com.ghintech.infor.logistics.model.M_INF_Picking;
import org.compiere.model.Query;
public class UpdateInOutPickingNumber extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		boolean complete_documents = MSysConfig.getBooleanValue("Infor_Logistics_Complete_InOut",false,getPO().getAD_Client_ID());
		String docstatuswhere="m_inout.docstatus='DR' or m_inout.docstatus='IP'";
		if(complete_documents)
			docstatuswhere="m_inout.docstatus='CO'";
		
		
		M_INF_Picking picking = (M_INF_Picking)  getPO();
		List<MInOut> listinout=new Query(Env.getCtx(), MInOut.Table_Name, "m_inout.issotrx='Y' and ("+docstatuswhere+") and INF_Picking_ID=?", getPO().get_TrxName())
				.setClient_ID().setParameters(picking.get_ID()).list();
		for(MInOut inout : listinout) {
			inout.set_ValueOfColumn("n_guia_preparacion", picking.getValue());
			
			inout.saveEx(getPO().get_TrxName());
		}
		
	}

}
