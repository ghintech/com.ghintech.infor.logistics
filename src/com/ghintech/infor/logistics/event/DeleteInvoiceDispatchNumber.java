package com.ghintech.infor.logistics.event;

import org.compiere.model.MInvoice;

import com.ghintech.infor.logistics.base.CustomEvent;
public class DeleteInvoiceDispatchNumber extends CustomEvent {

	@Override
	protected void doHandleEvent() {
		// TODO Auto-generated method stub
		MInvoice invoice = (MInvoice)  getPO();
		if(invoice.get_ValueAsString("n_guia_despacho").trim().compareTo("")==0 && invoice.get_Value("INF_Dispatch_ID")!=null) {
			invoice.set_ValueOfColumn("INF_Dispatch_ID", null);
			invoice.set_ValueOfColumn("n_guia_despacho", null);
			invoice.saveEx();
		}
			
	}

}
